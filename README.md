Base de datos
    Para tener la base de datos solo hay que importar el archivo 
    'controllicencias.sql', a tu motor de base de datos MySql

Datos del usuario predeterminado:
    usuario: admin
    pass: 123

Hay dos secciones RRHH y Usuarios

En la seccion Usuarios se podran crear nuevos usuarios y darles los permisos
para las tareas que pueda realizar cada uno.

Pasos para la seccion de RRHH:
    1- Se deben cargar los empleados en la pestaña de legajos
    2- Luego en la pestaña de licencias anuales, generar las licencias. Con el
    boton 'Generar Licencias', se generan automaticamente las licencias del año
    corriente automaticamente y con los empleados que estan activos, cuidando 
    de que no se dupliquen para el mismo año. Tambien podra generar una en
    particular en dos años anteriores y posteriores
    3- Una vez generadas las licencias anules podra generar pedidos de vacaciones
    y dias a cuenta de licencia (los cuales son para solicitar un dia o mas, y
    no siete o mas dias), generando un formulario para que el empleado lo 
    complete a puño y letra
    4- Una vez que el empleado entrega el formulario completo debe buscar
    el pedido de vacaciones por su numero de formulario o cualquier dato del
    empleado, pero respetando que el pedido que eliga modificar y el que tenga
    en mano sean el mismo. Para determinar la fecha del dicho pedido de 
    vacaciones