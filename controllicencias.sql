-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2020 a las 00:17:34
-- Versión del servidor: 5.5.39
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `controllicencias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE IF NOT EXISTS `auditoria` (
`id` int(10) NOT NULL,
  `usuario` tinytext,
  `accion` tinytext,
  `fecha` datetime DEFAULT NULL,
  `tabla` tinytext,
  `idregistro` int(10) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`id`, `usuario`, `accion`, `fecha`, `tabla`, `idregistro`) VALUES
(1, 'admin', 'Agregar', '2020-03-05 04:00:00', 'empleado', 1),
(2, 'admin', 'Modificar', '2020-03-05 04:07:00', 'empleado', 1),
(3, 'admin', 'Agregar', '2020-03-05 04:07:00', 'licenciavacaciones', 1),
(4, 'admin', 'Modificar', '2020-03-05 04:08:00', 'licenciavacaciones', 1),
(5, 'admin', 'Modificar', '2020-03-05 04:08:00', 'licenciavacaciones', 1),
(6, 'admin', 'Agregar', '2020-03-05 04:08:00', 'licenciavacaciones', 2),
(7, 'admin', 'Agregar', '2020-03-05 04:09:00', 'pedidovacaciones', 1),
(8, 'admin', 'Agregar', '2020-03-05 04:21:00', 'pedidovacaciones', 2),
(9, 'admin', 'Modificar', '2020-03-05 04:23:00', 'licenciavacaciones', 1),
(10, 'admin', 'Modificar', '2020-03-05 04:23:00', 'pedidovacaciones', 1),
(11, 'admin', 'Modificar', '2020-03-05 07:26:00', 'licenciavacaciones', 2),
(12, 'admin', 'Modificar', '2020-03-05 07:26:00', 'pedidovacaciones', 2),
(13, 'admin', 'Agregar', '2020-03-05 07:41:00', 'diasctalicencia', 1),
(14, 'admin', 'Modificar', '2020-03-05 07:50:00', 'licenciavacaciones', 2),
(15, 'admin', 'Modificar', '2020-03-05 07:50:00', 'diasctalicencia', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `delegacion`
--

CREATE TABLE IF NOT EXISTS `delegacion` (
`id` int(3) NOT NULL,
  `nombre` tinytext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `delegacion`
--

INSERT INTO `delegacion` (`id`, `nombre`) VALUES
(1, 'Centro'),
(2, 'Guaymallen'),
(3, 'Maipu'),
(4, 'Lujan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diasctalicencia`
--

CREATE TABLE IF NOT EXISTS `diasctalicencia` (
`id` int(10) NOT NULL,
  `fecha` date DEFAULT NULL,
  `desde` date DEFAULT NULL,
  `hasta` date DEFAULT NULL,
  `dias` int(2) DEFAULT NULL,
  `idempleado` int(10) DEFAULT NULL,
  `idlicenciavacaciones` int(10) DEFAULT NULL,
  `pendientes` int(2) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `diasctalicencia`
--

INSERT INTO `diasctalicencia` (`id`, `fecha`, `desde`, `hasta`, `dias`, `idempleado`, `idlicenciavacaciones`, `pendientes`) VALUES
(1, '2020-03-05', '2020-03-30', '2020-03-30', 1, 1, 2, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE IF NOT EXISTS `empleado` (
`legajo` int(8) NOT NULL,
  `ingreso` date DEFAULT NULL,
  `idpersona` int(10) DEFAULT NULL,
  `idseccion` int(3) DEFAULT NULL,
  `iddelegacion` int(3) DEFAULT NULL,
  `baja` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`legajo`, `ingreso`, `idpersona`, `idseccion`, `iddelegacion`, `baja`) VALUES
(1, '2000-01-01', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licenciavacaciones`
--

CREATE TABLE IF NOT EXISTS `licenciavacaciones` (
`id` int(10) NOT NULL,
  `anio` tinytext,
  `dias` int(2) DEFAULT NULL,
  `pendientes` tinyint(2) DEFAULT NULL,
  `idempleado` int(10) DEFAULT NULL,
  `antiguedad` tinytext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `licenciavacaciones`
--

INSERT INTO `licenciavacaciones` (`id`, `anio`, `dias`, `pendientes`, `idempleado`, `antiguedad`) VALUES
(1, '2020/2021', 35, 21, 1, '20 aÃ±os, 11 meses'),
(2, '2019/2020', 28, 13, 1, '19 aÃ±os, 11 meses');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liquidacion`
--

CREATE TABLE IF NOT EXISTS `liquidacion` (
`id` int(10) NOT NULL,
  `mes` int(2) DEFAULT NULL,
  `dias` int(3) DEFAULT NULL,
  `liquidado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `liquidacion`
--

INSERT INTO `liquidacion` (`id`, `mes`, `dias`, `liquidado`) VALUES
(1, 2, 14, 1),
(2, 2, 14, 0),
(3, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liquidaciondiasctalicencia`
--

CREATE TABLE IF NOT EXISTS `liquidaciondiasctalicencia` (
`id` int(10) NOT NULL,
  `idliquidacion` int(10) NOT NULL,
  `iddiasctalicencia` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `liquidaciondiasctalicencia`
--

INSERT INTO `liquidaciondiasctalicencia` (`id`, `idliquidacion`, `iddiasctalicencia`) VALUES
(1, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liquidacionpedidovacaciones`
--

CREATE TABLE IF NOT EXISTS `liquidacionpedidovacaciones` (
`id` int(10) NOT NULL,
  `idliquidacion` int(10) NOT NULL,
  `idpedidovacaciones` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `liquidacionpedidovacaciones`
--

INSERT INTO `liquidacionpedidovacaciones` (`id`, `idliquidacion`, `idpedidovacaciones`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidovacaciones`
--

CREATE TABLE IF NOT EXISTS `pedidovacaciones` (
`id` int(10) NOT NULL,
  `fecha` date DEFAULT NULL,
  `desde` date DEFAULT NULL,
  `hasta` date DEFAULT NULL,
  `dias` tinyint(2) DEFAULT NULL,
  `idlicenciavacaciones` int(10) DEFAULT NULL,
  `pendientes` tinyint(3) DEFAULT NULL,
  `idempleado` int(10) DEFAULT NULL,
  `autorizado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `pedidovacaciones`
--

INSERT INTO `pedidovacaciones` (`id`, `fecha`, `desde`, `hasta`, `dias`, `idlicenciavacaciones`, `pendientes`, `idempleado`, `autorizado`) VALUES
(1, '2020-03-05', '2020-03-02', '2020-03-15', 14, 1, 21, 1, 0),
(2, '2020-03-05', '2020-03-16', '2020-03-29', 14, 2, 14, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE IF NOT EXISTS `permiso` (
`id` int(10) NOT NULL,
  `descripcion` tinytext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `descripcion`) VALUES
(1, 'usuarios'),
(2, 'subsidios'),
(3, 'rrhh'),
(4, 'turismo'),
(5, 'linkLegajos'),
(6, 'agregarEmpleado'),
(7, 'modificarEmpleado'),
(8, 'eliminarEmpleado'),
(9, 'AuditoriaEmpleados'),
(10, 'linkVacaciones'),
(11, 'linkPedidoVacaciones'),
(12, 'agregarPedidoVacaciones'),
(13, 'generarPedidoVacaciones'),
(14, 'modificarPedidoVacaciones'),
(15, 'eliminarPedidoVacaciones'),
(16, 'autorizarPedidoVacaciones'),
(17, 'AuditoriaPedidoVacaciones'),
(18, 'linkDiasCtaLicencia'),
(19, 'agregarDiasCtaLicencia'),
(20, 'modificarDiasCtaLicencia'),
(21, 'eliminarDiasCtaLicencia'),
(22, 'autorizarDiasCtaLicencia'),
(23, 'AuditoriaDiasCtaLicencia'),
(24, 'linkLicenciaVacaciones'),
(25, 'agregarLicenciaVacaciones'),
(26, 'generarLicenciasVacaciones'),
(27, 'modificarLicenciaVacaciones'),
(28, 'AuditoriaLicenciaVacaciones'),
(29, 'agregarUsuario'),
(30, 'modificarUsuario'),
(31, 'auditoriaUsuario'),
(32, 'linkLiquidacion'),
(33, 'modificarLiquidacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
`id` int(10) NOT NULL,
  `dni` int(10) NOT NULL,
  `apellido` tinytext,
  `nombre` tinytext,
  `sexo` char(1) DEFAULT NULL,
  `cuil` tinytext,
  `nacimiento` date DEFAULT NULL,
  `telefono` tinytext,
  `domicilio` text,
  `localidad` tinytext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `dni`, `apellido`, `nombre`, `sexo`, `cuil`, `nacimiento`, `telefono`, `domicilio`, `localidad`) VALUES
(1, 12345678, 'Perez', 'Juan', 'M', '27123456781', '1970-01-01', '2615432100', 'Calle 123', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

CREATE TABLE IF NOT EXISTS `seccion` (
`id` int(3) NOT NULL,
  `nombre` tinytext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `seccion`
--

INSERT INTO `seccion` (`id`, `nombre`) VALUES
(1, 'Administracion'),
(2, 'Finanzas'),
(3, 'Publicidad'),
(4, 'Informatica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`id` int(10) NOT NULL,
  `nombre` tinytext NOT NULL,
  `clave` text NOT NULL,
  `baja` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `clave`, `baja`) VALUES
(1, 'admin', 'MTIz', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariopermiso`
--

CREATE TABLE IF NOT EXISTS `usuariopermiso` (
`id` int(10) NOT NULL,
  `idusuario` int(10) DEFAULT NULL,
  `idpermiso` int(10) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Volcado de datos para la tabla `usuariopermiso`
--

INSERT INTO `usuariopermiso` (`id`, `idusuario`, `idpermiso`) VALUES
(1, 1, 3),
(2, 1, 1),
(55, 1, 5),
(56, 1, 6),
(57, 1, 29),
(59, 1, 30),
(60, 1, 10),
(61, 1, 11),
(62, 1, 12),
(63, 1, 18),
(64, 1, 19),
(65, 1, 24),
(66, 1, 7),
(67, 1, 14),
(68, 1, 20),
(69, 1, 25),
(70, 1, 8),
(71, 1, 9),
(72, 1, 13),
(73, 1, 15),
(74, 1, 17),
(75, 1, 21),
(76, 1, 23),
(77, 1, 26),
(78, 1, 27),
(79, 1, 28),
(81, 1, 32),
(82, 1, 33);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `delegacion`
--
ALTER TABLE `delegacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `diasctalicencia`
--
ALTER TABLE `diasctalicencia`
 ADD PRIMARY KEY (`id`), ADD KEY `idempleado` (`idempleado`), ADD KEY `idlicenciavacaciones` (`idlicenciavacaciones`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
 ADD PRIMARY KEY (`legajo`), ADD KEY `empleado_ibseccion` (`idseccion`), ADD KEY `empleado_ibdelegacion` (`iddelegacion`), ADD KEY `empleado_ibpersona` (`idpersona`);

--
-- Indices de la tabla `licenciavacaciones`
--
ALTER TABLE `licenciavacaciones`
 ADD PRIMARY KEY (`id`), ADD KEY `licenciavacaciones_ibempleado` (`idempleado`);

--
-- Indices de la tabla `liquidacion`
--
ALTER TABLE `liquidacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `liquidaciondiasctalicencia`
--
ALTER TABLE `liquidaciondiasctalicencia`
 ADD PRIMARY KEY (`id`), ADD KEY `liquidaciondiasctalicencia_idliquidacion` (`idliquidacion`), ADD KEY `liquidaciondiasctalicencia_iddiasctalicencia` (`iddiasctalicencia`);

--
-- Indices de la tabla `liquidacionpedidovacaciones`
--
ALTER TABLE `liquidacionpedidovacaciones`
 ADD PRIMARY KEY (`id`), ADD KEY `liquidacionpedidovacaciones_idliquidacion` (`idliquidacion`), ADD KEY `liquidacionpedidovacaciones_idpedidovacaciones` (`idpedidovacaciones`);

--
-- Indices de la tabla `pedidovacaciones`
--
ALTER TABLE `pedidovacaciones`
 ADD PRIMARY KEY (`id`), ADD KEY `pedidovacaciones_ibempleado` (`idempleado`), ADD KEY `pedidovacaciones_iblicenciavacaciones` (`idlicenciavacaciones`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indices de la tabla `seccion`
--
ALTER TABLE `seccion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuariopermiso`
--
ALTER TABLE `usuariopermiso`
 ADD PRIMARY KEY (`id`), ADD KEY `usuariopermiso_ibusuario` (`idusuario`), ADD KEY `usuariopermiso_ibpermiso` (`idpermiso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `delegacion`
--
ALTER TABLE `delegacion`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `diasctalicencia`
--
ALTER TABLE `diasctalicencia`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
MODIFY `legajo` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `licenciavacaciones`
--
ALTER TABLE `licenciavacaciones`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `liquidacion`
--
ALTER TABLE `liquidacion`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `liquidaciondiasctalicencia`
--
ALTER TABLE `liquidaciondiasctalicencia`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `liquidacionpedidovacaciones`
--
ALTER TABLE `liquidacionpedidovacaciones`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pedidovacaciones`
--
ALTER TABLE `pedidovacaciones`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `seccion`
--
ALTER TABLE `seccion`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuariopermiso`
--
ALTER TABLE `usuariopermiso`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `diasctalicencia`
--
ALTER TABLE `diasctalicencia`
ADD CONSTRAINT `diasctalicencia_ibfk_1` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`legajo`),
ADD CONSTRAINT `diasctalicencia_ibfk_2` FOREIGN KEY (`idlicenciavacaciones`) REFERENCES `licenciavacaciones` (`id`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
ADD CONSTRAINT `empleado_ibdelegacion` FOREIGN KEY (`iddelegacion`) REFERENCES `delegacion` (`id`),
ADD CONSTRAINT `empleado_ibpersona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`id`),
ADD CONSTRAINT `empleado_ibseccion` FOREIGN KEY (`idseccion`) REFERENCES `seccion` (`id`);

--
-- Filtros para la tabla `licenciavacaciones`
--
ALTER TABLE `licenciavacaciones`
ADD CONSTRAINT `licenciavacaciones_ibempleado` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`legajo`);

--
-- Filtros para la tabla `liquidaciondiasctalicencia`
--
ALTER TABLE `liquidaciondiasctalicencia`
ADD CONSTRAINT `liquidaciondiasctalicencia_iddiasctalicencia` FOREIGN KEY (`iddiasctalicencia`) REFERENCES `diasctalicencia` (`id`),
ADD CONSTRAINT `liquidaciondiasctalicencia_idliquidacion` FOREIGN KEY (`idliquidacion`) REFERENCES `liquidacion` (`id`);

--
-- Filtros para la tabla `liquidacionpedidovacaciones`
--
ALTER TABLE `liquidacionpedidovacaciones`
ADD CONSTRAINT `liquidacionpedidovacaciones_idliquidacion` FOREIGN KEY (`idliquidacion`) REFERENCES `liquidacion` (`id`),
ADD CONSTRAINT `liquidacionpedidovacaciones_idpedidovacaciones` FOREIGN KEY (`idpedidovacaciones`) REFERENCES `pedidovacaciones` (`id`);

--
-- Filtros para la tabla `pedidovacaciones`
--
ALTER TABLE `pedidovacaciones`
ADD CONSTRAINT `pedidovacaciones_ibempleado` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`legajo`),
ADD CONSTRAINT `pedidovacaciones_iblicenciavacaciones` FOREIGN KEY (`idlicenciavacaciones`) REFERENCES `licenciavacaciones` (`id`);

--
-- Filtros para la tabla `usuariopermiso`
--
ALTER TABLE `usuariopermiso`
ADD CONSTRAINT `usuariopermiso_ibpermiso` FOREIGN KEY (`idpermiso`) REFERENCES `permiso` (`id`),
ADD CONSTRAINT `usuariopermiso_ibusuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
